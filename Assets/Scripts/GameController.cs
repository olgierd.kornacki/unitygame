using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public static GameController instance;

    public int maxGems;
    public int currentGems;

    [SerializeField] private GameObject playerObject;
    [SerializeField] private Transform spawnPoint;

    private bool cRunning = false;

    [SerializeField] private GameObject[] GemsList;

    public TMPro.TextMeshProUGUI pointText;

    void Start()
    {
        if (instance == null & instance != this)
            instance = this;

        if (playerObject == null) playerObject = GameObject.FindGameObjectWithTag("Player");
        if (spawnPoint == null) spawnPoint = GameObject.Find("SpawnPoint").transform;

        GemsList = GameObject.FindGameObjectsWithTag("Gem");
    }

  
    void Update()
    {
        if(currentGems == maxGems)
        {
            ResetGame();
        }

        if (playerObject.transform.position.y < -3f)
        {
            if (!cRunning)
                StartCoroutine(ChangeObjectPosition());
        }
    }
    private void ResetGame()
    {
        currentGems = 0;
        if (!cRunning)
            StartCoroutine(ChangeObjectPosition());
        foreach(var gem in GemsList)
        {
            gem.SetActive(true);
        }
        pointText.text = "0/" + maxGems.ToString();
    }

    IEnumerator ChangeObjectPosition()
    {
        cRunning = true;
        CharacterController controller = playerObject.GetComponent<CharacterController>();
        controller.enabled = false;
        playerObject.transform.position = spawnPoint.position;
        controller.enabled = true;
        yield return null;
        cRunning = false;
    }
}
